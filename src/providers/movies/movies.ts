import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class MoviesProvider {

  constructor(public http: HttpClient) {}

  getMovies(token){
    return this.http.get(`https://galaxyapp-7a9de.firebaseio.com/movies.json?auth=${token}`)
  }

}
