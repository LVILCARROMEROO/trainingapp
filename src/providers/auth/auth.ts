import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthProvider {
  apiKey:string = 'AIzaSyAA3t6dcMAWR7zXzAZwLvg83TOFBDX2PwE';

  constructor(public http: HttpClient) {}

  singIn(email,password){
    const body = {
      email,
      password,
      returnSecureToken:true
    };
    return this.http.post(`https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=${this.apiKey}`,body)
      .toPromise()
  }

  singUp(email,password){
    const body = {
      email,
      password,
      returnSecureToken:true
    };
    return this.http.post(`https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=${this.apiKey}`,body)
      .toPromise()
  }


}
