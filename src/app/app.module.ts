import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {MoviesPage} from "../pages/movies/movies";
import {AuthPage} from "../pages/auth/auth";
import {MovieDetailPage} from "../pages/movie-detail/movie-detail";
import { MoviesProvider } from '../providers/movies/movies';
import {HttpClientModule} from "@angular/common/http";
import { AuthProvider } from '../providers/auth/auth';
import {IonicStorageModule} from "@ionic/storage";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AuthPage,
    MoviesPage,
    MovieDetailPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AuthPage,
    MoviesPage,
    MovieDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MoviesProvider,
    AuthProvider
  ]
})
export class AppModule {}
