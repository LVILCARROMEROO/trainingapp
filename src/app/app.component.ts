import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import {AuthPage} from "../pages/auth/auth";
import {Storage} from "@ionic/storage";
import {MoviesPage} from "../pages/movies/movies";
import {User} from "../pages/interfaces/user";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    storage: Storage
  ) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();

      storage.get('user')
        .then((user:User)=>{
          if (user) this.rootPage = MoviesPage;
          else this.rootPage = AuthPage;
        })
    });
  }
}

