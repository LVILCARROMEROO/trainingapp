export interface User {
  email:String;
  expiresIn:String;
  idToken:String;
  kind:String
  localId:String
  refreshToken:String;
}
