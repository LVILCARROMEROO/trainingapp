export interface Movie {
  id: number,
  vote_average: number,
  title: String,
  poster_path: String,
  original_title: String,
  genres: String[],
  backdrop_path: String,
  overview: String,
  release_date: String,
  videos: {url:String,name:String}[]
}
