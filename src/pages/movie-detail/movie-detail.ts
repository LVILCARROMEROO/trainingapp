import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Movie} from "../interfaces/movie";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'page-movie-detail',
  templateUrl: 'movie-detail.html',
})
export class MovieDetailPage {
  movie:Movie;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public domSanitizer: DomSanitizer
  ) {}

  ionViewDidLoad() {
    this.movie = this.navParams.data;
  }

  setBackground(url){
    return `url(${url})`
  }

  secureUrl(url){
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url)
  }

}
