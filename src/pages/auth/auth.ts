import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import {MoviesPage} from "../movies/movies";
import {AuthProvider} from "../../providers/auth/auth";
import {Storage} from "@ionic/storage";
import {User} from "../interfaces/user";


@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
})
export class AuthPage {
  email;
  password;
  confirmPassword;
  singUpMode:boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authProvider: AuthProvider,
    private storage: Storage,
    public alertCtrl: AlertController
  ) {
  }

  ionViewDidLoad() {
  }

  toogleMode(){
    this.singUpMode = !this.singUpMode;
  }

  singIn(){
    this.authProvider.singIn(this.email,this.password)
      .then((result:User)=>{
        this.storage.set('user',result)
          .then(()=>{
            this.navCtrl.setRoot(MoviesPage)
          })
      })
      .catch((error:any)=>{
        console.log(error)
        this.showAlert('Error',error.error.error.message)
      })
  }

  singUp(){
    this.authProvider.singUp(this.email,this.password)
      .then((result:User)=>{
        this.storage.set('user',result)
          .then(()=>{
            this.navCtrl.setRoot(MoviesPage)
          })
      })
      .catch((error:any)=>{
        console.log(error)
        this.showAlert('Error',error.error.error.message)
      })
  }

  showAlert(title,message){
    this.alertCtrl.create({
      title,
      message,
      buttons:[
        {text:'Ok'}
      ]
    }).present()
  }

}
