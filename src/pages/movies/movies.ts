import { Component } from '@angular/core';
import {AlertController, NavController, NavParams} from 'ionic-angular';
import {MovieDetailPage} from "../movie-detail/movie-detail";
import {MoviesProvider} from "../../providers/movies/movies";
import {Movie} from "../interfaces/movie";
import {Storage} from "@ionic/storage";
import {User} from "../interfaces/user";
import {AuthPage} from "../auth/auth";

@Component({
  selector: 'page-movies',
  templateUrl: 'movies.html',
})
export class MoviesPage {
  movies:Movie[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public moviesProvider: MoviesProvider,
    private storage:Storage,
    public alertCtrl:AlertController
  ) {

  }

  ionViewDidLoad() {
    this.storage.get('user')
      .then((user:User)=>{
        if (user) {
          this.moviesProvider.getMovies(user.idToken)
            .subscribe((movies:Movie[]) => this.movies = movies)
        }
      });
  }

  goDetail(movie:Movie){
    this.navCtrl.push(MovieDetailPage,movie)
  }

  logout(){
    this.alertCtrl.create({
      title:'Warning',
      message:'Do you want to exit?',
      buttons:[
        {
          text:'No',
          role:'cancel',
          handler:()=>{}
        },
        {
          text:'Yes',
          handler:()=>{
            this.storage.remove('user')
              .then(()=>{
                this.navCtrl.setRoot(AuthPage)
              })
          }
        }
      ]
    }).present()
  }
}
